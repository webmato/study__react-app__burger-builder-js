/* -------------------------------------------------( imports )------------------------------------------------------ */
// Dependencies
import * as PropTypes from 'prop-types';
import * as React from 'react';

// Modules
import Backdrop from '../Backdrop';

// Assets
import classes from './Modal.module.css';


/* ------------------------------------------------( variables )----------------------------------------------------- */


/* ------------------------------------------------( component )----------------------------------------------------- */
class Modal extends React.Component {
  static propTypes = {
    children: PropTypes.node.isRequired,
    handleClickCloseModal: PropTypes.func.isRequired,
    isModalVisible: PropTypes.bool.isRequired,
  };

  static resolveShouldComponentUpdate(nextProps, isVisible, children) {
    return nextProps.isVisible !== isVisible || nextProps.children !== children;
  }

  shouldComponentUpdate(nextProps) {
    return Modal.resolveShouldComponentUpdate(nextProps, this.props.isModalVisible, this.props.children);
  }

  render() {
    const modalStyle = {
      opacity: this.props.isModalVisible ? 1 : 0,
      transform: this.props.isModalVisible ? 'translateY(0)' : 'translateY(-100vh)',
    };

    return (
      <>
        <Backdrop isVisible={this.props.isModalVisible} handleClickBackdrop={this.props.handleClickCloseModal} />
        <div className={classes.Modal} style={modalStyle}>{this.props.children}</div>
      </>
    );
  }
}


/* -------------------------------------------------( exports )------------------------------------------------------ */
export default Modal;
