/* -------------------------------------------------( imports )------------------------------------------------------ */
// Dependencies
import * as PropTypes from 'prop-types';
import * as React from 'react';

// Modules

// Assets
import classes from './Backdrop.module.css';


/* ------------------------------------------------( variables )----------------------------------------------------- */


/* -------------------------------------------------( methods )------------------------------------------------------ */


/* ------------------------------------------------( component )----------------------------------------------------- */
const Backdrop = props => (
  props.isVisible
    ? <div role="presentation" className={classes.Backdrop} onClick={props.handleClickBackdrop} />
    : null
);

Backdrop.propTypes = {
  handleClickBackdrop: PropTypes.func.isRequired,
  isVisible: PropTypes.bool.isRequired,
};


/* -------------------------------------------------( exports )------------------------------------------------------ */
export default Backdrop;
