/* -------------------------------------------------( imports )------------------------------------------------------ */
// Dependencies
import * as PropTypes from 'prop-types';
import * as React from 'react';

// Modules

// Assets
import classes from './FormElement.module.css';


/* ------------------------------------------------( variables )----------------------------------------------------- */
export const ELEMENT_TYPES = {
  input: 'input',
  select: 'select',
  textArea: 'textarea',
};

export const INPUT_TYPES = {
  email: 'email',
  password: 'password',
  text: 'text',
};


/* -------------------------------------------------( methods )------------------------------------------------------ */
export function getElementClasses(elmClasses, isTouched, isInvalid) {
  return isTouched && !isInvalid ? [elmClasses, classes.Invalid].join(' ') : elmClasses;
}

export function getElementNode(elementType, elementConfig, isTouched, isValid, value, handleChangeFormElement) {
  switch (elementType) {
    case ELEMENT_TYPES.input: {
      return (
        <input {...elementConfig}
               className={getElementClasses(classes.InputElement, isTouched, isValid)}
               value={value}
               onChange={handleChangeFormElement} />
      );
    }

    case ELEMENT_TYPES.textArea: {
      return (
        <textarea {...elementConfig}
                  className={getElementClasses(classes.InputElement, isTouched, isValid)}
                  value={value}
                  onChange={handleChangeFormElement} />
      );
    }

    case ELEMENT_TYPES.select: {
      const { options, id } = elementConfig;
      return (
        <select id={id}
                className={getElementClasses(classes.InputElement, isTouched, isValid)}
                value={value}
                onChange={handleChangeFormElement}>
          {
            Object.entries(options).map((option) => {
              const [optionKey, optionName] = option;
              return <option key={optionKey} value={optionKey}>{optionName}</option>;
            })
          }
        </select>
      );
    }

    default: {
      return (
        <input {...elementConfig} className={classes.InputElement} value={value} onChange={handleChangeFormElement} />
      );
    }
  }
}

/* ------------------------------------------------( component )----------------------------------------------------- */
const FormElement = props => (
  <div className={classes.Input}>
    {
      props.label
        ? (<label htmlFor={props.elementConfig.id} className={classes.Label}>{props.label}</label>)
        : null
    }
    {getElementNode(
      props.elementType,
      props.elementConfig,
      props.isTouched,
      props.isValid,
      props.value,
      props.handleChangeFormElement,
    )}
  </div>
);


FormElement.defaultProps = {
  isTouched: false,
  isValid: true,
  label: null,
  value: null,
};

FormElement.propTypes = {
  elementConfig: PropTypes.shape({
    id: PropTypes.string.isRequired,
    inputType: PropTypes.string,
  }).isRequired,
  elementType: PropTypes.oneOf(Object.values(ELEMENT_TYPES)).isRequired,
  label: PropTypes.string,
  handleChangeFormElement: PropTypes.func.isRequired,
  isTouched: PropTypes.bool,
  isValid: PropTypes.bool,
  value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
};


/* -------------------------------------------------( exports )------------------------------------------------------ */
export default FormElement;
