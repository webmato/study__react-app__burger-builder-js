/* -------------------------------------------------( imports )------------------------------------------------------ */
// Dependencies
import * as PropTypes from 'prop-types';
import * as React from 'react';

// Modules
import TEXT_PANEL_TYPE, { TEXT_PANEL_TYPE__ARRAY } from './TextPanel.constants';

// Assets
import classes from './TextPanel.module.css';


/* ------------------------------------------------( variables )----------------------------------------------------- */


/* -------------------------------------------------( methods )------------------------------------------------------ */
function getTextPanelClasses(type) {
  const typeClass = classes[type];
  return [classes.TextPanel, typeClass].join(' ');
}

/* ------------------------------------------------( component )----------------------------------------------------- */
const TextPanel = props => (<div className={getTextPanelClasses(props.type)}>{props.children}</div>);

TextPanel.defaultProps = {
  type: TEXT_PANEL_TYPE.info,
};

TextPanel.propTypes = {
  children: PropTypes.node.isRequired,
  type: PropTypes.oneOf(TEXT_PANEL_TYPE__ARRAY),
};


/* -------------------------------------------------( exports )------------------------------------------------------ */
export default TextPanel;
