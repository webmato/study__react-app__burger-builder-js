const TEXT_PANEL_TYPE = {
  info: 'info',
  warning: 'warning',
  danger: 'danger',
};

export const TEXT_PANEL_TYPE__ARRAY = Object.values(TEXT_PANEL_TYPE);

export default TEXT_PANEL_TYPE;
