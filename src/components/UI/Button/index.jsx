/* -------------------------------------------------( imports )------------------------------------------------------ */
// Dependencies
import * as PropTypes from 'prop-types';
import * as React from 'react';

// Assets
import classes from './Button.module.css';

// Modules


/* ------------------------------------------------( variables )----------------------------------------------------- */
export const BUTTON_STYLES = {
  success: 'Success',
  danger: 'Danger',
};
export const BUTTON_TYPES = {
  button: 'button',
  submit: 'submit',
};

/* -------------------------------------------------( methods )------------------------------------------------------ */
export function getButtonClasses(buttonType) {
  return [classes.Button, buttonType ? classes[buttonType] : ''].join(' ');
}


/* ------------------------------------------------( component )----------------------------------------------------- */
const Button = props => (
  <button type={props.buttonType} // eslint-disable-line react/button-has-type
          className={getButtonClasses(props.buttonStyle)}
          onClick={props.handleClickButton}
          disabled={props.isDisabled}>
    {props.children}
  </button>
);

Button.defaultProps = {
  buttonType: 'button',
  buttonStyle: null,
  isDisabled: false,
  handleClickButton: null,
};

Button.propTypes = {
  isDisabled: PropTypes.bool,
  buttonType: PropTypes.string,
  buttonStyle: PropTypes.string,
  children: PropTypes.node.isRequired,
  handleClickButton: PropTypes.func,
};


/* -------------------------------------------------( exports )------------------------------------------------------ */
export default Button;
