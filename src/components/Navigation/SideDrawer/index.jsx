/* -------------------------------------------------( imports )------------------------------------------------------ */
// Dependencies
import * as PropTypes from 'prop-types';
import * as React from 'react';

// Modules
import Logo from '../../Logo';
import Backdrop from '../../UI/Backdrop';
import Navigation from '../Navigation';

// Assets
import classes from './SideDrawer.module.css';


/* ------------------------------------------------( variables )----------------------------------------------------- */


/* -------------------------------------------------( methods )------------------------------------------------------ */
export function getSideDrawerClasses(sideDrawerIsVisible) {
  return [classes.SideDrawer, sideDrawerIsVisible ? classes.Open : classes.Close].join(' ');
}


/* ------------------------------------------------( component )----------------------------------------------------- */
const SideDrawer = props => (
  <>
    <Backdrop handleClickBackdrop={props.handleClickSideDrawerClose} isVisible={props.isSideDrawerVisible} />
    <div className={getSideDrawerClasses(props.isSideDrawerVisible)}>
      <div className={classes.Logo}>
        <Logo />
      </div>
      <Navigation isAuthenticated={props.isAuthenticated} />
    </div>
  </>
);

SideDrawer.defaultProps = {
  isAuthenticated: false,
};


SideDrawer.propTypes = {
  handleClickSideDrawerClose: PropTypes.func.isRequired,
  isAuthenticated: PropTypes.bool,
  isSideDrawerVisible: PropTypes.bool.isRequired,
};


/* -------------------------------------------------( exports )------------------------------------------------------ */
export default SideDrawer;
