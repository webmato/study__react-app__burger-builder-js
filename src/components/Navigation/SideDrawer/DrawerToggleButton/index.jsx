/* -------------------------------------------------( imports )------------------------------------------------------ */
// Dependencies
import * as PropTypes from 'prop-types';
import * as React from 'react';

// Modules

// Assets
import classes from './DrawerToggleButton.module.css';


/* ------------------------------------------------( variables )----------------------------------------------------- */


/* -------------------------------------------------( methods )------------------------------------------------------ */
export function getClasses(propsClasses = '') {
  return [classes.DrawerToggleButton, propsClasses].join(' ').toString();
}

/* ------------------------------------------------( component )----------------------------------------------------- */
const DrawerToggleButton = props => (
  <div role="presentation" className={getClasses(props.classes)} onClick={props.handleClickDrawerToggle}>
    <div />
    <div />
    <div />
  </div>
);

DrawerToggleButton.defaultProps = {
  classes: null,
};

DrawerToggleButton.propTypes = {
  handleClickDrawerToggle: PropTypes.func.isRequired,
  classes: PropTypes.string,
};


/* -------------------------------------------------( exports )------------------------------------------------------ */
export default DrawerToggleButton;
