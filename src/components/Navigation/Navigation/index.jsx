/* -------------------------------------------------( imports )------------------------------------------------------ */
// Dependencies
import * as PropTypes from 'prop-types';
import * as React from 'react';

// Modules
import NavigationItems from './NavigationItems';

// Assets
import classes from './Navigation.module.css';


/* ------------------------------------------------( variables )----------------------------------------------------- */


/* -------------------------------------------------( methods )------------------------------------------------------ */
export function getNavigationClasses(customClasses) {
  return [classes.Navigation, customClasses || null].join(' ');
}


/* ------------------------------------------------( component )----------------------------------------------------- */
const Navigation = props => (
  <nav className={getNavigationClasses(props.classes)}>
    <NavigationItems isAuthenticated={props.isAuthenticated} />
  </nav>
);


Navigation.defaultProps = {
  classes: null,
  isAuthenticated: false,
};

Navigation.propTypes = {
  classes: PropTypes.string,
  isAuthenticated: PropTypes.bool,
};


/* -------------------------------------------------( exports )------------------------------------------------------ */
export default Navigation;
