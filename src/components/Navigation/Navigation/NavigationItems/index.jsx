/* -------------------------------------------------( imports )------------------------------------------------------ */
import * as PropTypes from 'prop-types';
// Dependencies
import * as React from 'react';

// Modules
import NavigationItem from './NavigationItem';

// Assets
import classes from './NavigationItems.module.css';

// Constants
import { APP_LINKS } from '../../../../App.constants';


/* ------------------------------------------------( variables )----------------------------------------------------- */


/* -------------------------------------------------( methods )------------------------------------------------------ */


/* ------------------------------------------------( component )----------------------------------------------------- */
const NavigationItems = props => (
  <ul className={classes.NavigationItems}>
    <NavigationItem link={APP_LINKS.burgerBuilder}>Burger Builder</NavigationItem>
    {props.isAuthenticated
      ? (<NavigationItem link={APP_LINKS.orders}>Orders</NavigationItem>)
      : null
    }
    {!props.isAuthenticated
      ? (<NavigationItem link={APP_LINKS.auth}>Authentication</NavigationItem>)
      : (<NavigationItem link={APP_LINKS.logOut}>Log Out</NavigationItem>)
    }
  </ul>
);

NavigationItems.defaultProps = {
  isAuthenticated: false,
};

NavigationItems.propTypes = {
  isAuthenticated: PropTypes.bool,
};


/* -------------------------------------------------( exports )------------------------------------------------------ */
export default NavigationItems;
