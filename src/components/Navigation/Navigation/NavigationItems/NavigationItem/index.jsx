/* -------------------------------------------------( imports )------------------------------------------------------ */
// Dependencies
import * as PropTypes from 'prop-types';
import * as React from 'react';

// Modules
import { NavLink } from 'react-router-dom';

// Assets
import classes from './NavigationItem.module.css';

// Constants
import { APP_LINKS__VALUES } from '../../../../../App.constants';


/* ------------------------------------------------( variables )----------------------------------------------------- */


/* -------------------------------------------------( methods )------------------------------------------------------ */


/* ------------------------------------------------( component )----------------------------------------------------- */
const NavigationItem = props => (
  <li className={classes.NavigationItem}>
    <NavLink exact to={props.link} activeClassName={classes.active}>{props.children}</NavLink>
  </li>
);

NavigationItem.propTypes = {
  children: PropTypes.node.isRequired,
  link: PropTypes.oneOf(APP_LINKS__VALUES).isRequired,
};


/* -------------------------------------------------( exports )------------------------------------------------------ */
export default NavigationItem;
