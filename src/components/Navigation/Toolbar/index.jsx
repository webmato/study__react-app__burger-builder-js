/* -------------------------------------------------( imports )------------------------------------------------------ */
// Dependencies
import * as PropTypes from 'prop-types';
import * as React from 'react';

// Modules
import Logo from '../../Logo';
import Navigation from '../Navigation';
import DrawerToggleButton from '../SideDrawer/DrawerToggleButton';

// Assets
import classes from './Toolbar.module.css';


/* ------------------------------------------------( variables )----------------------------------------------------- */


/* -------------------------------------------------( methods )------------------------------------------------------ */


/* ------------------------------------------------( component )----------------------------------------------------- */
const Toolbar = props => (
  <header className={classes.Toolbar}>
    <DrawerToggleButton classes={classes.MobileOnly} handleClickDrawerToggle={props.handleClickSideDrawerToggle} />
    <div className={classes.Logo}>
      <Logo />
    </div>
    <Navigation classes={classes.DesktopOnly} isAuthenticated={props.isAuthenticated} />
  </header>
);

Toolbar.defaultProps = {
  isAuthenticated: false,
};

Toolbar.propTypes = {
  isAuthenticated: PropTypes.bool,
  handleClickSideDrawerToggle: PropTypes.func.isRequired,
};

/* -------------------------------------------------( exports )------------------------------------------------------ */
export default Toolbar;
