/* -------------------------------------------------( imports )------------------------------------------------------ */
// Dependencies
import * as PropTypes from 'prop-types';
import * as React from 'react';

// Modules
import ControlUnit from './ControlUnit';
import {
  ingredientsCountListPropsType,
  ingredientsEnabledListPropsType,
} from '../../containers/BurgerBuilder/BurgerBuilder.propTypes';

// Constants

// Assets
import classes from './ControlPanel.module.css';


/* ------------------------------------------------( variables )----------------------------------------------------- */


/* -------------------------------------------------( methods )------------------------------------------------------ */
export function getControlUnitsNode(
  ingredientName,
  enabledIngredient,
  count,
  reactKey,
  handleClickIngredientAdd,
  handleClickIngredientRemove,
) {
  return (
    <ControlUnit key={reactKey}
                 count={count}
                 controlUnitLabel={ingredientName}
                 isEnabledIngredientRemoveButton={enabledIngredient}
                 handleClickIngredientAdd={() => handleClickIngredientAdd(ingredientName)}
                 handleClickIngredientRemove={() => handleClickIngredientRemove(ingredientName)} />
  );
}

export function getControlUnitsNodes(
  listOfIngredients,
  enabledIngredients,
  handleClickIngredientAdd,
  handleClickIngredientRemove,
) {
  const listOfIngredientsArray = Object.keys(listOfIngredients);

  return listOfIngredientsArray.map(ingredientName => getControlUnitsNode(
    ingredientName,
    enabledIngredients[ingredientName],
    listOfIngredients[ingredientName],
    ingredientName,
    handleClickIngredientAdd,
    handleClickIngredientRemove,
  ));
}


/* ------------------------------------------------( component )----------------------------------------------------- */
const ControlPanel = props => (
  <div className={classes.BuildControls}>
    <div>
      Total price:
      <strong>
        ${props.totalPrice.toFixed(2)}
      </strong>
    </div>
    {getControlUnitsNodes(
      props.listOfIngredients,
      props.enabledIngredients,
      props.handleClickIngredientAdd,
      props.handleClickIngredientRemove,
    )}
    <button type="button"
            className={classes.OrderButton}
            onClick={props.handleClickOrderButton}
            disabled={!props.isPurchasable}>ORDER NOW</button>
  </div>
);

ControlPanel.propTypes = {
  enabledIngredients: ingredientsEnabledListPropsType.isRequired,
  handleClickIngredientAdd: PropTypes.func.isRequired,
  handleClickOrderButton: PropTypes.func.isRequired,
  handleClickIngredientRemove: PropTypes.func.isRequired,
  isPurchasable: PropTypes.bool.isRequired,
  listOfIngredients: ingredientsCountListPropsType.isRequired,
  totalPrice: PropTypes.number.isRequired,
};


/* -------------------------------------------------( exports )------------------------------------------------------ */
export default ControlPanel;
