/* -------------------------------------------------( imports )------------------------------------------------------ */
// Dependencies
import * as PropTypes from 'prop-types';
import * as React from 'react';

// Modules

// Assets
import classes from './ControlUnit.module.css';


/* ------------------------------------------------( variables )----------------------------------------------------- */


/* -------------------------------------------------( methods )------------------------------------------------------ */


/* ------------------------------------------------( component )----------------------------------------------------- */
const ControlUnit = props => (
  <div className={classes.ControlUnit}>
    <div className={classes.Label}>{props.controlUnitLabel}: {props.count}</div>
    <button type="button"
            className={classes.More}
            onClick={props.handleClickIngredientAdd}>+</button>
    <button type="button"
            className={classes.Less}
            onClick={props.handleClickIngredientRemove}
            disabled={!props.isEnabledIngredientRemoveButton}>-</button>
  </div>
);

ControlUnit.defaultProps = {
  count: 0,
  isEnabledIngredientRemoveButton: false,
};

ControlUnit.propTypes = {
  controlUnitLabel: PropTypes.string.isRequired,
  count: PropTypes.number,
  isEnabledIngredientRemoveButton: PropTypes.bool,
  handleClickIngredientRemove: PropTypes.func.isRequired,
  handleClickIngredientAdd: PropTypes.func.isRequired,
};


/* -------------------------------------------------( exports )------------------------------------------------------ */
export default ControlUnit;
