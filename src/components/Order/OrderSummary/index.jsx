/* -------------------------------------------------( imports )------------------------------------------------------ */
// Dependencies
import * as PropTypes from 'prop-types';
import * as React from 'react';

// Modules
import Burger from '../../Burger';
import Button, { BUTTON_STYLES } from '../../UI/Button';
import { ingredientsCountListPropsType } from '../../../containers/BurgerBuilder/BurgerBuilder.propTypes';


// Assets
import classes from './OrderSummary.module.css';


/* ------------------------------------------------( variables )----------------------------------------------------- */


/* -------------------------------------------------( methods )------------------------------------------------------ */
export function getControlButtonsNode(handleClickOrderCancel, handleClickOrderContinue) {
  return (
    <div>
      <Button buttonStyle={BUTTON_STYLES.danger} handleClickButton={handleClickOrderCancel}>CANCEL</Button>
      <Button buttonStyle={BUTTON_STYLES.success} handleClickButton={handleClickOrderContinue}>CONTINUE</Button>
    </div>
  );
}

/* ------------------------------------------------( component )----------------------------------------------------- */
const OrderSummary = props => (
  <div className={classes.OrderSummary}>
    <div className={classes.Burger}>
      <Burger selectedIngredients={props.ingredients} />
      <h1>We hope it tastes well!</h1>
      <p>Total Price: {props.totalPrice ? props.totalPrice.toFixed(2) : null}</p>
      {!props.isLoading ? getControlButtonsNode(props.handleClickOrderCancel, props.handleClickOrderContinue) : null}
    </div>
  </div>
);

OrderSummary.defaultProps = {
  ingredients: null,
  totalPrice: null,
};

OrderSummary.propTypes = {
  ingredients: ingredientsCountListPropsType,
  isLoading: PropTypes.bool.isRequired,
  totalPrice: PropTypes.number,
  handleClickOrderCancel: PropTypes.func.isRequired,
  handleClickOrderContinue: PropTypes.func.isRequired,
};


/* -------------------------------------------------( exports )------------------------------------------------------ */
export default OrderSummary;
