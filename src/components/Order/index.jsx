/* -------------------------------------------------( imports )------------------------------------------------------ */
// Dependencies
import * as PropTypes from 'prop-types';
import * as React from 'react';

// Modules

// Assets
import classes from './Order.module.css';


/* ------------------------------------------------( variables )----------------------------------------------------- */


/* -------------------------------------------------( methods )------------------------------------------------------ */
export function parseIngredients(ingredientsObj) {
  if (ingredientsObj) {
    return Object.entries(ingredientsObj).map((ingredient, index) => {
      const [name, count] = ingredient;
      if (count) {
        const separator = index ? ', ' : '';
        return `${separator}${name} (${count})`;
      }
      return null;
    });
  }
  return 'No ingredients found';
}

export function parseOrderDate(orderDate) {
  const date = new Date(orderDate);
  return `${date.toDateString()}, ${date.toLocaleTimeString()}`;
}

/* ------------------------------------------------( component )----------------------------------------------------- */
const Order = props => (
  <div className={classes.Order}>
    <p>{parseOrderDate(props.orderData.date)}</p>
    <p>User: {props.orderData.orderDetails.name}</p>
    <p>Ingredients: {parseIngredients(props.orderData.ingredients)}</p>
    <p>Price: <strong>${props.orderData.totalPrice.toFixed(2)}</strong></p>
  </div>
);


Order.propTypes = {
  orderData: PropTypes.shape({
    date: PropTypes.number.isRequired,
    ingredients: PropTypes.object.isRequired,
    totalPrice: PropTypes.number.isRequired,
    orderDetails: PropTypes.shape({
      name: PropTypes.string.isRequired,
    }).isRequired,
  }).isRequired,
};


/* -------------------------------------------------( exports )------------------------------------------------------ */
export default Order;
