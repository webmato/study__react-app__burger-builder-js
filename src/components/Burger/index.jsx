/* -------------------------------------------------( imports )------------------------------------------------------ */
// Dependencies
import * as React from 'react';

// Modules
import BurgerIngredient from './BurgerIngredient';
import { ingredientsCountListPropsType } from '../../containers/BurgerBuilder/BurgerBuilder.propTypes';

// Constants
import { BURGER_INGREDIENTS__MANDATORY } from './BurgerIngredient/BurgerIngredient.constants';

// Assets
import classes from './Burger.module.css';


/* ------------------------------------------------( variables )----------------------------------------------------- */


/* -------------------------------------------------( methods )------------------------------------------------------ */
export function resolveArrayOfSelectedIngredients(ingredients) {
  if (ingredients) {
    const ingredientsKeys = Object.keys(ingredients);

    return ingredientsKeys.reduce((accumulatorArray, ingredientKey) => {
      const count = ingredients[ingredientKey];
      const nestedArrayOfIngredients = Array(count)
        .fill(ingredientKey);
      return [...accumulatorArray, ...nestedArrayOfIngredients];
    }, []);
  }

  return null;
}

export function getBurgerIngredientNode(ingredient, elmKey = null) {
  if (elmKey) {
    return <BurgerIngredient key={elmKey} ingredientType={ingredient} />;
  }
  return <BurgerIngredient ingredientType={ingredient} />;
}

export function getSelectedBurgerIngredientNodes(selectedIngredients) {
  const arrayOfSelectedIngredients = resolveArrayOfSelectedIngredients(selectedIngredients);
  if (arrayOfSelectedIngredients && arrayOfSelectedIngredients.length > 0) {
    return arrayOfSelectedIngredients.map(
      (ingredient, index) => getBurgerIngredientNode(ingredient, ingredient + index),
    );
  }

  return <em>Please start adding ingredients!</em>;
}


/* ------------------------------------------------( component )----------------------------------------------------- */
const Burger = props => (
  <div className={classes.Burger}>
    {getBurgerIngredientNode(BURGER_INGREDIENTS__MANDATORY.breadTop)}
    {getSelectedBurgerIngredientNodes(props.selectedIngredients)}
    {getBurgerIngredientNode(BURGER_INGREDIENTS__MANDATORY.breadBottom)}
  </div>
);

Burger.defaultProps = {
  selectedIngredients: null,
};

Burger.propTypes = {
  selectedIngredients: ingredientsCountListPropsType,
};


/* -------------------------------------------------( exports )------------------------------------------------------ */
export default Burger;
