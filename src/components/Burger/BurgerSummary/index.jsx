/* -------------------------------------------------( imports )------------------------------------------------------ */
// Dependencies
import * as PropTypes from 'prop-types';
import * as React from 'react';

// Modules
import Button, { BUTTON_STYLES } from '../../UI/Button';
import { ingredientsCountListPropsType } from '../../../containers/BurgerBuilder/BurgerBuilder.propTypes';

// Assets
import classes from './BurgerSummary.module.css';


/* ------------------------------------------------( variables )----------------------------------------------------- */


/* ------------------------------------------------( component )----------------------------------------------------- */

/*
 *  This component could be a pure functional component.
 * Its turned to class component because of educational/debugging reasons.
 * See the componentWillUpdate() method. It is updating even the component is not visible.
 *
 * We will solve this by shouldComponentUpdate() in parent Modal component.
 */
class BurgerSummary extends React.Component {
  static propTypes = {
    handleClickCancelCheckOut: PropTypes.func.isRequired,
    handleClickContinueCheckOut: PropTypes.func.isRequired,
    ingredients: ingredientsCountListPropsType.isRequired,
    totalPrice: PropTypes.number.isRequired,
  };

  static getListItem(ingredientName, count) {
    return (
      <li className={classes.ListItem} key={ingredientName}>
        <span>{ingredientName}: {count}</span>
      </li>
    );
  }

  static getListItems(ingredients) {
    return ingredients.map(([ingredientName, count]) => BurgerSummary.getListItem(ingredientName, count));
  }

  getListItems = ingredients => BurgerSummary.getListItems(Object.entries(ingredients));

  render() {
    return (
      <>
        <h3>Your Order</h3>
        <p>Delicious burger with following ingredients:</p>
        <ul>
          {this.getListItems(this.props.ingredients)}
        </ul>
        <p>
          <strong>Total Price: $ {this.props.totalPrice.toFixed(2)}</strong>
        </p>
        <p>Continue to checkout?</p>
        <Button buttonStyle={BUTTON_STYLES.danger}
                handleClickButton={this.props.handleClickCancelCheckOut}>CANCEL</Button>
        <Button buttonStyle={BUTTON_STYLES.success}
                handleClickButton={this.props.handleClickContinueCheckOut}>CONTINUE</Button>
      </>
    );
  }
}


/* -------------------------------------------------( exports )------------------------------------------------------ */
export default BurgerSummary;
