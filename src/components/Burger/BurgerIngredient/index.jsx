/* -------------------------------------------------( imports )------------------------------------------------------ */
// Dependencies
import * as PropTypes from 'prop-types';
import * as React from 'react';

// Modules

// Constants
import {
  BURGER_INGREDIENTS__MANDATORY,
  BURGER_INGREDIENTS__OPTIONAL,
  BURGER_INGREDIENTS__ALL__KEBAB_CASE,
} from './BurgerIngredient.constants';

// Assets
import classes from './BurgerIngredient.module.css';


/* ------------------------------------------------( variables )----------------------------------------------------- */


/* -------------------------------------------------( methods )------------------------------------------------------ */
export function getBurgerIngredientNode(ingredientType) {
  switch (ingredientType) {
    case (BURGER_INGREDIENTS__MANDATORY.breadBottom):
      return <div className={classes.BreadBottom} />;

    case (BURGER_INGREDIENTS__MANDATORY.breadTop):
      return (
        <div className={classes.BreadTop}>
          <div className={classes.Seeds1} />
          <div className={classes.Seeds2} />
        </div>
      );

    case (BURGER_INGREDIENTS__OPTIONAL.meat):
      return <div className={classes.Meat} />;

    case (BURGER_INGREDIENTS__OPTIONAL.cheese):
      return <div className={classes.Cheese} />;

    case (BURGER_INGREDIENTS__OPTIONAL.bacon):
      return <div className={classes.Bacon} />;

    case (BURGER_INGREDIENTS__OPTIONAL.salad):
      return <div className={classes.Salad} />;

    default:
      return null;
  }
}


/* ------------------------------------------------( component )----------------------------------------------------- */
const BurgerIngredient = props => getBurgerIngredientNode(props.ingredientType);

BurgerIngredient.propTypes = {
  ingredientType: PropTypes.oneOf(BURGER_INGREDIENTS__ALL__KEBAB_CASE).isRequired,
};


/* -------------------------------------------------( exports )------------------------------------------------------ */
export default BurgerIngredient;
