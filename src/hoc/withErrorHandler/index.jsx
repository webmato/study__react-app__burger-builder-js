/* -------------------------------------------------( imports )------------------------------------------------------ */
// Dependencies
import * as React from 'react';
import Modal from '../../components/UI/Modal';

// Modules

// Assets


/* ------------------------------------------------( variables )----------------------------------------------------- */


/* -------------------------------------------------( methods )------------------------------------------------------ */
function resolveNextErrorState(error) {
  return { error };
}

function resolveNextCleanErrorState() {
  return { error: null };
}

export function handleClickCloseModalStatic() {
  return resolveNextCleanErrorState();
}

/* ------------------------------------------------( component )----------------------------------------------------- */
const withErrorHandler = (WrappedComponent, axios) => class extends React.Component {
  state = {
    error: null,
  };

  axiosInterceptors = {
    request: null,
    response: null,
  };

  componentWillMount() {
    const request = axios.interceptors.request.use((req) => {
      const nextState = resolveNextCleanErrorState();
      this.setState(nextState);
      return req;
    });

    const response = axios.interceptors.response.use(res => res, (error) => {
      const nextState = resolveNextErrorState(error);
      this.setState(nextState);
    });

    this.axiosInterceptors = { request, response };
  }

  componentWillUnmount() {
    axios.interceptors.request.eject(this.axiosInterceptors.request);
    axios.interceptors.response.eject(this.axiosInterceptors.response);
  }

  handleClickCloseModal = () => {
    this.setState(handleClickCloseModalStatic());
  };

  render() {
    return (
      <>
        <Modal isModalVisible={!!this.state.error} handleClickCloseModal={this.handleClickCloseModal}>
          {this.state.error ? this.state.error.message : ''}
        </Modal>
        <WrappedComponent {...this.props} />
      </>
    );
  }
};


/* -------------------------------------------------( exports )------------------------------------------------------ */
export default withErrorHandler;
