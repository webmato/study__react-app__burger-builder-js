/* -------------------------------------------------( imports )------------------------------------------------------ */
// Dependencies
import * as PropTypes from 'prop-types';
import * as React from 'react';
import { connect } from 'react-redux';

// Modules
import SideDrawer from '../../components/Navigation/SideDrawer';
import Toolbar from '../../components/Navigation/Toolbar';

// Assets
import classes from './Layout.module.css';


/* ------------------------------------------------( variables )----------------------------------------------------- */
const mapStateToProps = state => ({
  isAuthenticated: !!state.authState.authData.token,
});


/* ------------------------------------------------( component )----------------------------------------------------- */
class Layout extends React.Component {
  static defaultProps = {
    isAuthenticated: false,
  };

  static propTypes = {
    isAuthenticated: PropTypes.bool,
    children: PropTypes.oneOfType([PropTypes.node, PropTypes.arrayOf(PropTypes.node)]).isRequired,
  };

  static handleClickSideDrawerToggle(isSideDrawerVisible) {
    return { isSideDrawerVisible: !isSideDrawerVisible };
  }

  static handleClickSideDrawerClose() {
    return { isSideDrawerVisible: false };
  }

  state = {
    isSideDrawerVisible: false,
  };

  handleClickSideDrawerToggle = () => {
    this.setState(prevState => Layout.handleClickSideDrawerToggle(prevState.isSideDrawerVisible));
  };

  handleClickSideDrawerClose = () => {
    this.setState(Layout.handleClickSideDrawerClose());
  };

  render() {
    return (
      <>
        <Toolbar handleClickSideDrawerToggle={this.handleClickSideDrawerToggle}
                 isAuthenticated={this.props.isAuthenticated} />
        <SideDrawer handleClickSideDrawerClose={this.handleClickSideDrawerClose}
                    isAuthenticated={this.props.isAuthenticated}
                    isSideDrawerVisible={this.state.isSideDrawerVisible} />
        <main className={classes.Content}>{this.props.children}</main>
      </>
    );
  }
}


/* -------------------------------------------------( exports )------------------------------------------------------ */
export default connect(mapStateToProps)(Layout);
