/* -------------------------------------------------( imports )------------------------------------------------------ */
import { ACTION_TYPES } from '../actions/auth';


/* ------------------------------------------------( variables )----------------------------------------------------- */

const initialState = {
  isLoading: false,
  authData: {
    userId: null,
    token: null,
  },
  error: false,
  errorData: null,
};


/* -------------------------------------------------( helpers )------------------------------------------------------ */


/* -------------------------------------------------( reducer )------------------------------------------------------ */
export function userAuthRequest(state) {
  return {
    ...state,
    error: true,
    errorData: null,
    isLoading: true,
  };
}

export function userAuthSuccess(state, payload) {
  const { authData: { userId, token } } = payload;

  return {
    ...state,
    authData: {
      ...state.authData,
      userId,
      token,
    },
    error: true,
    errorData: null,
    isLoading: false,
  };
}

export function userAuthFailure(state, payload) {
  const { error } = payload;

  return {
    ...state,
    error: true,
    errorData: error,
    isLoading: false,
  };
}

export function userAuthLogOut(state) {
  return {
    ...state,
    authData: {
      ...state.authData,
      userId: initialState.authData.userId,
      token: initialState.authData.token,
    },
  };
}

function reducer(state = initialState, { type, payload }) {
  switch (type) {
    case ACTION_TYPES.USER_AUTH_REQUEST: { return userAuthRequest(state); }
    case ACTION_TYPES.USER_AUTH_SUCCESS: { return userAuthSuccess(state, payload); }
    case ACTION_TYPES.USER_AUTH_FAILURE: { return userAuthFailure(state, payload); }
    case ACTION_TYPES.USER_LOG_OUT: { return userAuthLogOut(state); }
    default: { return state; }
  }
}

/* -------------------------------------------------( exports )------------------------------------------------------ */
export default reducer;
