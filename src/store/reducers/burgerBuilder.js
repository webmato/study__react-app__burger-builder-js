/* -------------------------------------------------( imports )------------------------------------------------------ */
import {
  BURGER_INGREDIENTS_PRICES,
  BURGER_INGREDIENTS__OPTIONAL,
} from '../../components/Burger/BurgerIngredient/BurgerIngredient.constants';
import { ACTION_TYPES } from '../actions/burgerBuilder';


/* ------------------------------------------------( variables )----------------------------------------------------- */
export const initialState = {
  ingredients: null,
  isBurgerInConfirmProcess: false,
  isBuild: false,
  totalPrice: null,
  error: false,
  errorData: null,
};


/* -------------------------------------------------( helpers )------------------------------------------------------ */


/* -------------------------------------------------( reducer )------------------------------------------------------ */
export function builderInitialStateFailure(state, payload) {
  const { error } = payload;

  return {
    ...state,
    error: true,
    errorData: error,
  };
}

export function builderInitialStateSuccess(state, payload) {
  const { loadedInitState } = payload;

  return {
    ...state,
    isBuild: false,
    isBurgerInConfirmProcess: false,
    ...loadedInitState,
  };
}

export function burgerInConfirmProcess(state, payload) {
  const { isBurgerInConfirmProcess } = payload;

  return {
    ...state,
    isBurgerInConfirmProcess,
  };
}

export function ingredientAdd(state, payload) {
  const { ingredientName } = payload;

  return {
    ...state,
    ingredients: {
      ...state.ingredients,
      [ingredientName]: state.ingredients[ingredientName] + 1,
    },
    totalPrice: state.totalPrice + BURGER_INGREDIENTS_PRICES[BURGER_INGREDIENTS__OPTIONAL[ingredientName]],
    isBuild: true,
  };
}

export function ingredientRemove(state, payload) {
  const { ingredientName } = payload;

  return {
    ...state,
    ingredients: {
      ...state.ingredients,
      [ingredientName]: state.ingredients[ingredientName] - 1,
    },
    totalPrice: state.totalPrice - BURGER_INGREDIENTS_PRICES[BURGER_INGREDIENTS__OPTIONAL[ingredientName]],
    isBuild: true,
  };
}

function reducer(state = initialState, { type, payload }) {
  switch (type) {
    case ACTION_TYPES.INGREDIENT_ADD: { return ingredientAdd(state, payload); }
    case ACTION_TYPES.INGREDIENT_REMOVE: { return ingredientRemove(state, payload); }
    case ACTION_TYPES.BUILDER_INITIAL_STATE_SUCCESS: { return builderInitialStateSuccess(state, payload); }
    case ACTION_TYPES.BUILDER_INITIAL_STATE_FAILURE: { return builderInitialStateFailure(state, payload); }
    case ACTION_TYPES.BURGER_IN_CONFIRM_PROC: { return burgerInConfirmProcess(state, payload); }
    default: { return state; }
  }
}


/* -------------------------------------------------( exports )------------------------------------------------------ */
export default reducer;
