/* -------------------------------------------------( imports )------------------------------------------------------ */
import { ACTION_TYPES } from '../actions/orders';


/* ------------------------------------------------( variables )----------------------------------------------------- */
const initialState = {
  error: false,
  errorData: null,
  isOrderInCheckOutProcess: false,
  isLoading: false,
  orders: new Map(),
};


/* -------------------------------------------------( helpers )------------------------------------------------------ */


/* -------------------------------------------------( reducer )------------------------------------------------------ */
export function orderInCheckoutProcess(state, payload) {
  const { isOrderInCheckOutProcess } = payload;
  //
  return {
    ...state,
    isOrderInCheckOutProcess,
  };
}

export function orderCheckoutRequest(state) {
  return {
    ...state,
    isLoading: true,
  };
}

export function orderCheckoutSuccess(state, payload) {
  const { orderId, orderData } = payload;
  const newOrder = new Map([[orderId, orderData]]);
  //
  return {
    ...state,
    isLoading: false,
    isOrderInCheckOutProcess: false,
    orders: new Map([...state.orders, ...newOrder]),
  };
}

export function orderCheckoutFailure(state, payload) {
  const { error } = payload;
  //
  return {
    ...state,
    error: true,
    errorData: error,
    isLoading: false,
    isOrderInCheckOutProcess: false,
  };
}

export function ordersListGetRequest(state) {
  return {
    state,
    errorData: null,
    error: false,
    isLoading: true,
  };
}

export function ordersListGetSuccess(payload, state) {
  const { ordersList } = payload;
  //
  return {
    ...state,
    isLoading: false,
    isOrderInCheckOutProcess: false,
    orders: ordersList ? new Map(Object.entries(ordersList)) : new Map(),
  };
}

export function ordersListGetFailure(state, payload) {
  const { error } = payload;

  //
  return {
    ...state,
    error: true,
    errorData: error,
    isLoading: false,
    isOrderInCheckOutProcess: false,
  };
}


function reducer(state = initialState, { type, payload }) {
  switch (type) {
    case ACTION_TYPES.ORDER_IN_CHECKOUT_PROCESS: { return orderInCheckoutProcess(state, payload); }
    case ACTION_TYPES.ORDER_CHECKOUT_REQUEST: { return orderCheckoutRequest(state); }
    case ACTION_TYPES.ORDER_CHECKOUT_SUCCESS: { return orderCheckoutSuccess(state, payload); }
    case ACTION_TYPES.ORDER_CHECKOUT_FAILURE: { return orderCheckoutFailure(state, payload); }
    case ACTION_TYPES.ORDERS_LIST_GET_REQUEST: { return ordersListGetRequest(state); }
    case ACTION_TYPES.ORDERS_LIST_GET_SUCCESS: { return ordersListGetSuccess(payload, state); }
    case ACTION_TYPES.ORDERS_LIST_GET_FAILURE: { return ordersListGetFailure(state, payload); }
    default: { return state; }
  }
}

/* -------------------------------------------------( exports )------------------------------------------------------ */
export default reducer;
