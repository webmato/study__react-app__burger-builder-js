/* eslint-disable import/prefer-default-export */

export function updateObject(oldObject, ...newValues) {
  return {
    ...oldObject,
    ...Object.assign(...newValues), // convert the array of rest parameter to object and than spread again
  };
}
