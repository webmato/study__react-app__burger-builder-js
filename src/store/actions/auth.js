/* eslint-disable import/prefer-default-export */
/* -------------------------------------------------( imports )------------------------------------------------------ */
import axios from 'axios';

import { API_BASE_URL, LOCAL_STORAGE_KEYS } from '../../App.constants';

/* ------------------------------------------------( variables )----------------------------------------------------- */
export const ACTION_TYPES = {
  USER_AUTH_REQUEST: 'USER_AUTH_REQUEST',
  USER_AUTH_SUCCESS: 'USER_AUTH_SUCCESS',
  USER_AUTH_FAILURE: 'USER_AUTH_FAILURE',
  USER_LOG_OUT: 'USER_LOG_OUT',
};


/* -------------------------------------------------( helpers )------------------------------------------------------ */
export function parseUserAuthSuccessResponse(response) {
  const { data: { expiresIn, localId: userId, idToken: token } } = response;
  const expiresInMs = expiresIn * 1000;
  return { expiresInMs, userId, token };
}

export function parseUserAuthFailureResponse(error) {
  const { response: { data: { error: errorData } } } = error;
  return errorData;
}

export function calculateExpirationDate(expiresInMs) {
  return new Date(new Date().getTime() + expiresInMs);
}

export function calculateExpirationTimeout(actualDate, expirationDate) {
  return new Date(expirationDate).getTime() - new Date(actualDate).getTime();
}

export function setAuthDataToLocalStorage(expiresInMs, userId, token) {
  localStorage.setItem(LOCAL_STORAGE_KEYS.expirationDate, calculateExpirationDate(expiresInMs));
  localStorage.setItem(LOCAL_STORAGE_KEYS.token, token);
  localStorage.setItem(LOCAL_STORAGE_KEYS.userId, userId);
}

export function unsetAuthDataInLocalStorage() {
  localStorage.removeItem(LOCAL_STORAGE_KEYS.expirationDate);
  localStorage.removeItem(LOCAL_STORAGE_KEYS.token);
  localStorage.removeItem(LOCAL_STORAGE_KEYS.userId);
}

export function readAuthDataFromLocalStorage() {
  const expirationDate = new Date(localStorage.getItem(LOCAL_STORAGE_KEYS.expirationDate));
  const token = localStorage.getItem(LOCAL_STORAGE_KEYS.token);
  const userId = localStorage.getItem(LOCAL_STORAGE_KEYS.userId);
  return { expirationDate, token, userId };
}


/* ---------------------------------------------( action creators )-------------------------------------------------- */
export const userLogOut = () => {
  unsetAuthDataInLocalStorage();
  return { type: ACTION_TYPES.USER_LOG_OUT };
};
export const userLogOutTimer = expiresInMs => (dispatch) => {
  setTimeout(() => {
    dispatch(userLogOut());
  }, expiresInMs);
};

export const userAuthRequest = () => ({ type: ACTION_TYPES.USER_AUTH_REQUEST });
export const userAuthSuccess = authData => ({ type: ACTION_TYPES.USER_AUTH_SUCCESS, payload: { authData } });
export const userAuthFailure = error => ({ type: ACTION_TYPES.USER_AUTH_FAILURE, payload: { error } });
export const userAuthInit = (email, password, isSignUpMode) => (dispatch) => {
  dispatch(userAuthRequest());
  const authData = { email, password, returnSecureToken: true };
  const authUrl = isSignUpMode ? API_BASE_URL.auth.email.signUp : API_BASE_URL.auth.email.logIn;

  axios.post(authUrl, authData)
    .then((response) => {
      const authResponseData = parseUserAuthSuccessResponse(response);
      const { expiresInMs, userId, token } = authResponseData;
      setAuthDataToLocalStorage(expiresInMs, userId, token);
      dispatch(userAuthSuccess({ userId, token }));
      dispatch(userLogOutTimer(expiresInMs));
    })
    .catch((error) => {
      dispatch(userAuthFailure(parseUserAuthFailureResponse(error)));
    });
};
export const userAuthTryAutoLogIn = () => (dispatch) => {
  const { expirationDate, token, userId } = readAuthDataFromLocalStorage();
  if (token && userId && expirationDate) {
    const actualDate = new Date();

    if (expirationDate > actualDate) {
      const expiresInMs = calculateExpirationTimeout(actualDate, expirationDate);
      dispatch(userAuthSuccess({ userId, token }));
      dispatch(userLogOutTimer(expiresInMs));
    } else {
      dispatch(userLogOut());
    }
  }
};
