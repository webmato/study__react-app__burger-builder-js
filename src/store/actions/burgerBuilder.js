/* -------------------------------------------------( imports )------------------------------------------------------ */
import { API_URL } from '../../App.constants';
import axios from '../../axios/axios-orders';


/* ------------------------------------------------( variables )----------------------------------------------------- */
export const ACTION_TYPES = {
  INGREDIENT_ADD: 'INGREDIENT_ADD',
  INGREDIENT_REMOVE: 'INGREDIENT_REMOVE',

  BUILDER_INITIAL_STATE_FAILURE: 'BUILDER_INITIAL_STATE_FAILURE',
  BUILDER_INITIAL_STATE_SUCCESS: 'BUILDER_INITIAL_STATE_SUCCESS',

  BURGER_IN_CONFIRM_PROC: 'BURGER_IN_CONFIRM_PROC',
};


/* -------------------------------------------------( helpers )------------------------------------------------------ */
export function parseBuilderInitialStateResponse(response) {
  const { data: loadedInitState } = response;
  return loadedInitState;
}


/* ---------------------------------------------( action creators )-------------------------------------------------- */
export const ingredientAdd = ingredientName => ({
  type: ACTION_TYPES.INGREDIENT_ADD,
  payload: { ingredientName },
});
export const ingredientRemove = ingredientName => ({
  type: ACTION_TYPES.INGREDIENT_REMOVE,
  payload: { ingredientName },
});

export const builderInitialStateSuccess = loadedInitState => ({
  type: ACTION_TYPES.BUILDER_INITIAL_STATE_SUCCESS,
  payload: { loadedInitState },
});
export const builderInitialStateFailure = error => ({
  type: ACTION_TYPES.BUILDER_INITIAL_STATE_FAILURE,
  payload: { error },
});
export const builderInitialStateRequest = () => (dispatch) => {
  axios.get(API_URL.settings.GET)
    .then(response => dispatch(builderInitialStateSuccess(parseBuilderInitialStateResponse(response))))
    .catch(error => dispatch(builderInitialStateFailure(error)));
};

export const burgerInConfirmProcess = isBurgerInConfirmProcess => ({
  type: ACTION_TYPES.BURGER_IN_CONFIRM_PROC,
  payload: { isBurgerInConfirmProcess },
});
