/* -------------------------------------------------( imports )------------------------------------------------------ */
import { API_URL } from '../../App.constants';
import axios from '../../axios/axios-orders';


/* ------------------------------------------------( variables )----------------------------------------------------- */
export const ACTION_TYPES = {
  ORDER_IN_CHECKOUT_PROCESS: 'ORDER_IN_CHECKOUT_PROCESS',

  ORDER_CHECKOUT_REQUEST: 'ORDER_CHECKOUT_REQUEST',
  ORDER_CHECKOUT_SUCCESS: 'ORDER_CHECKOUT_SUCCESS',
  ORDER_CHECKOUT_FAILURE: 'ORDER_CHECKOUT_FAILURE',

  ORDERS_LIST_GET_REQUEST: 'ORDERS_LIST_GET_REQUEST',
  ORDERS_LIST_GET_SUCCESS: 'ORDERS_LIST_GET_SUCCESS',
  ORDERS_LIST_GET_FAILURE: 'ORDERS_LIST_GET_FAILURE',
};


/* -------------------------------------------------( helpers )------------------------------------------------------ */
export function parseBurgerCheckoutPostResponse(response) {
  const { data: { name: orderId } } = response;
  return orderId;
}

export function parseOrdersListIsFetchResponse(response) {
  const { data } = response;
  return data;
}

export function parseErrorResponse(error) {
  const { response: { data: errorData } } = error;
  return errorData;
}

export function resolveOrdersRequestUrl(urlPath, queryString) {
  return queryString ? `${urlPath}?${queryString}` : urlPath;
}

export function resolveOrdersCheckoutPostUrl(urlPath, token) {
  const queryString = new URLSearchParams({
    auth: token,
  }).toString();

  return resolveOrdersRequestUrl(urlPath, queryString);
}

export function resolveOrdersListGetUrl(urlPath, token, userId) {
  const queryString = new URLSearchParams({
    auth: token,
    orderBy: '"userId"',
    equalTo: `"${userId}"`,
  }).toString();

  return resolveOrdersRequestUrl(urlPath, queryString);
}


/* ---------------------------------------------( action creators )-------------------------------------------------- */
export const orderInCheckoutProcess = isOrderInCheckOutProcess => ({
  type: ACTION_TYPES.ORDER_IN_CHECKOUT_PROCESS,
  payload: { isOrderInCheckOutProcess },
});

export const orderCheckoutRequest = () => ({ type: ACTION_TYPES.ORDER_CHECKOUT_REQUEST });
export const orderCheckoutFailure = error => ({ type: ACTION_TYPES.ORDER_CHECKOUT_FAILURE, payload: { error } });
export const orderCheckoutSuccess = (orderId, orderData) => ({
  type: ACTION_TYPES.ORDER_CHECKOUT_SUCCESS,
  payload: { orderId, orderData },
});
export const orderCheckoutInit = (orderData, token) => (dispatch) => {
  dispatch(orderCheckoutRequest());
  //
  axios.post(resolveOrdersCheckoutPostUrl(API_URL.order.POST, token), orderData)
    .then(response => dispatch(orderCheckoutSuccess(parseBurgerCheckoutPostResponse(response), orderData)))
    .catch(error => dispatch(orderCheckoutFailure(parseErrorResponse(error))));
};

export const ordersListGetRequest = () => ({ type: ACTION_TYPES.ORDERS_LIST_GET_REQUEST });
export const ordersListGetFailure = error => ({ type: ACTION_TYPES.ORDERS_LIST_GET_FAILURE, payload: { error } });
export const ordersListGetSuccess = ordersList => ({
  type: ACTION_TYPES.ORDERS_LIST_GET_SUCCESS,
  payload: { ordersList },
});
export const ordersListGetInit = (token, userId) => (dispatch) => {
  dispatch(ordersListGetRequest());
  //
  axios.get(resolveOrdersListGetUrl(API_URL.ordersList.GET, token, userId))
    .then(response => dispatch(ordersListGetSuccess(parseOrdersListIsFetchResponse(response))))
    .catch(error => dispatch(ordersListGetFailure(parseErrorResponse(error))));
};
