/* -------------------------------------------------( imports )------------------------------------------------------ */
// Dependencies
import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { BrowserRouter } from 'react-router-dom';
import { Provider } from 'react-redux';
import {
  applyMiddleware,
  combineReducers,
  compose,
  createStore,
} from 'redux';
import thunk from 'redux-thunk';

// Modules
import * as serviceWorker from './serviceWorker';
import burgerBuilderReducer from './store/reducers/burgerBuilder';
import ordersReducer from './store/reducers/orders';
import authReducer from './store/reducers/auth';
import App from './App';

// Assets
import './index.css';

/* ------------------------------------------------( variables )----------------------------------------------------- */
const rootReducer = combineReducers({
  authState: authReducer,
  burgerBuilderState: burgerBuilderReducer,
  ordersState: ordersReducer,
});
const reduxDevToolOptions = { serialize: { options: { map: true } } };
const composeEnhancers = (window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ && window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__(reduxDevToolOptions)) || compose; // eslint-disable-line no-underscore-dangle, max-len
const middleware = [thunk];
const store = createStore(
  rootReducer,
  composeEnhancers(applyMiddleware(...middleware)),
);

/* ------------------------------------------------( component )----------------------------------------------------- */
ReactDOM.render(
  (
    <Provider store={store}>
      <BrowserRouter>
        <App />
      </BrowserRouter>
    </Provider>
  ),
  document.getElementById('root'),
);


/* ------------------------------------------------( utilities )----------------------------------------------------- */
// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
