/* -------------------------------------------------( imports )------------------------------------------------------ */
// Dependencies
import * as PropTypes from 'prop-types';
import * as React from 'react';
import { connect } from 'react-redux';
import {
  Redirect,
  Route,
  Switch,
  withRouter,
} from 'react-router-dom';

// Modules
import { APP_LINKS } from './App.constants';
import Auth from './containers/Auth';
import BurgerBuilder from './containers/BurgerBuilder';
import Checkout from './containers/Checkout';
import LogOut from './containers/LogOut';
import Orders from './containers/Orders/Orders';
import Layout from './hoc/Layout';
import * as actions from './store/actions';

// Assets


/* ------------------------------------------------( variables )----------------------------------------------------- */
const mapDispatchToProps = dispatch => ({
  tryUserAuthFromLocalStorage: () => dispatch(actions.userAuthTryAutoLogIn()),
});

const mapStateToProps = state => ({
  isAuthenticated: !!state.authState.authData.token,
});


/* -------------------------------------------------( methods )------------------------------------------------------ */


/* ------------------------------------------------( component )----------------------------------------------------- */
class App extends React.Component {
  static defaultProps = {
    isAuthenticated: false,
  };

  static propTypes = {
    tryUserAuthFromLocalStorage: PropTypes.func.isRequired,
    isAuthenticated: PropTypes.bool,
  };

  static getRouteNodes(isAuthenticated) {
    if (isAuthenticated) {
      return (
        <Switch>
          <Route path={APP_LINKS.checkOut} component={Checkout} />
          <Route path={APP_LINKS.orders} component={Orders} />
          <Route path={APP_LINKS.logOut} component={LogOut} />
          <Route path={APP_LINKS.burgerBuilder} exact component={BurgerBuilder} />
          <Redirect to="/" />
        </Switch>
      );
    }

    return (
      <Switch>
        <Route path={APP_LINKS.auth} component={Auth} />
        <Route path={APP_LINKS.burgerBuilder} exact component={BurgerBuilder} />
        <Redirect to="/" />
      </Switch>
    );
  }

  componentDidMount() {
    this.props.tryUserAuthFromLocalStorage();
  }

  render() {
    return (
      <div>
        <Layout>
          {App.getRouteNodes(this.props.isAuthenticated)}
        </Layout>
      </div>
    );
  }
}


/* -------------------------------------------------( exports )------------------------------------------------------ */
export default withRouter(connect(mapStateToProps, mapDispatchToProps)(App));
