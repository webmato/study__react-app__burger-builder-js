/* -------------------------------------------------( imports )------------------------------------------------------ */
// Dependencies
import * as React from 'react';
import * as PropTypes from 'prop-types';
import { connect } from 'react-redux';

// Modules
// import axios from '../../axios/axios-orders';
import Burger from '../../components/Burger';
import ControlPanel from '../../components/ControlPanel';
import BurgerSummary from '../../components/Burger/BurgerSummary';
import Modal from '../../components/UI/Modal';
import Spinner from '../../components/UI/Spinner';
import TextPanel from '../../components/UI/TextPanel';
import TEXT_PANEL_TYPE from '../../components/UI/TextPanel/TextPanel.constants';
// import withErrorHandler from '../../hoc/withErrorHandler';

// Constants
// import { BURGER_DELIVERY_METHODS } from './BurgerBuilder.constants';
import { APP_LINKS } from '../../App.constants';
import * as actions from '../../store/actions';
import { ingredientsCountListPropsType } from './BurgerBuilder.propTypes';

// Assets


/* ------------------------------------------------( variables )----------------------------------------------------- */
const mapStateToProps = state => ({
  error: state.burgerBuilderState.error,
  ingredients: state.burgerBuilderState.ingredients,
  isAuthenticated: !!state.authState.authData.token,
  isBuild: state.burgerBuilderState.isBuild,
  isBurgerInConfirmProcess: state.burgerBuilderState.isBurgerInConfirmProcess,
  totalPrice: state.burgerBuilderState.totalPrice,
});

const mapDispatchToProps = dispatch => ({
  addIngredient: ingredientName => dispatch(actions.ingredientAdd(ingredientName)),
  removeIngredient: ingredientName => dispatch(actions.ingredientRemove(ingredientName)),
  getInitialBuilderState: () => dispatch(actions.builderInitialStateRequest()),
  setOrderInCheckoutProcess: isOrderInCheckOutProcess => dispatch(actions.orderInCheckoutProcess(isOrderInCheckOutProcess)), // eslint-disable-line max-len
  setBurgerInConfirmProcess: isBurgerInConfirmProcess => dispatch(actions.burgerInConfirmProcess(isBurgerInConfirmProcess)), // eslint-disable-line max-len
});


/* -------------------------------------------------( methods )------------------------------------------------------ */


/* ------------------------------------------------( component )----------------------------------------------------- */
class BurgerBuilder extends React.Component {
  static defaultProps = {
    ingredients: null,
    totalPrice: null,
  };

  static propTypes = {
    addIngredient: PropTypes.func.isRequired,
    error: PropTypes.bool.isRequired,
    getInitialBuilderState: PropTypes.func.isRequired,
    history: PropTypes.shape({
      push: PropTypes.func.isRequired,
    }).isRequired,
    ingredients: ingredientsCountListPropsType,
    isAuthenticated: PropTypes.bool.isRequired,
    isBuild: PropTypes.bool.isRequired,
    isBurgerInConfirmProcess: PropTypes.bool.isRequired,
    removeIngredient: PropTypes.func.isRequired,
    setBurgerInConfirmProcess: PropTypes.func.isRequired,
    setOrderInCheckoutProcess: PropTypes.func.isRequired,
    totalPrice: PropTypes.number,
  };

  static componentDidMountStatic(
    getInitialBuilderState,
    isAuthenticated,
    isBurgerInConfirmProcess,
    isBuild,
    setBurgerInConfirmProcess,
  ) {
    if (!isBuild) {
      getInitialBuilderState();
    }

    if (!isAuthenticated) {
      setBurgerInConfirmProcess(false);
    }
  }

  static isPurchasable(ingredients) {
    const selectedIngredientsArr = Object.entries(ingredients);
    const sum = selectedIngredientsArr.reduce((result, [ingredient, count]) => result + count, 0); // eslint-disable-line no-unused-vars
    return sum > 0;
  }

  static getBurgerNode(ingredients) {
    if (ingredients) {
      return <Burger selectedIngredients={ingredients} />;
    }
    return null;
  }

  static getBurgerAndControlPanelNodes(
    error,
    ingredients,
    isAuthenticated,
    totalPrice,
    handleClickIngredientAdd,
    handleClickIngredientRemove,
    handleClickOrderButton,
  ) {
    if (ingredients && totalPrice) {
      return (
        <>
          {BurgerBuilder.getBurgerNode(ingredients)}
          {BurgerBuilder.getControlPanelNode(
            ingredients,
            isAuthenticated,
            totalPrice,
            handleClickIngredientAdd,
            handleClickIngredientRemove,
            handleClickOrderButton,
          )}
        </>
      );
    }
    return error ? (<TextPanel type={TEXT_PANEL_TYPE.danger}>An Error occurred :(</TextPanel>) : <Spinner />;
  }

  static getControlPanelNode(
    ingredients,
    isAuthenticated,
    totalPrice,
    handleClickIngredientAdd,
    handleClickIngredientRemove,
    handleClickOrderButton,
  ) {
    if (ingredients && totalPrice) {
      return (
        <ControlPanel
          enabledIngredients={BurgerBuilder.resolveEnabledIngredients(ingredients)}
          isPurchasable={BurgerBuilder.isPurchasable(ingredients)}
          listOfIngredients={ingredients}
          handleClickIngredientAdd={handleClickIngredientAdd}
          handleClickIngredientRemove={handleClickIngredientRemove}
          handleClickOrderButton={handleClickOrderButton}
          totalPrice={totalPrice} />
      );
    }
    return null;
  }

  static handleClickOrderButton(setBurgerInConfirmProcess, isAuthenticated, history) {
    setBurgerInConfirmProcess(true);

    if (!isAuthenticated) {
      history.push(APP_LINKS.auth);
    }
  }

  static handleClickContinueCheckOut(setOrderInCheckoutProcess, history) {
    setOrderInCheckoutProcess(true);
    BurgerBuilder.routeToCheckoutPage(history);
  }

  static handleClickCancelCheckOut(setBurgerInConfirmProc) {
    setBurgerInConfirmProc(false);
  }

  static handleClickIngredientAdd(addIngredient, ingredientName) {
    addIngredient(ingredientName);
  }

  static handleClickIngredientRemove(removeIngredient, ingredientName) {
    removeIngredient(ingredientName);
  }

  static getOrderSummaryNodes(ingredients, totalPrice, handleClickContinueCheckOut, handleClickCancelCheckOut) {
    if (!ingredients || !totalPrice) {
      return <Spinner />;
    }
    return (
      <BurgerSummary
        ingredients={ingredients}
        handleClickContinueCheckOut={handleClickContinueCheckOut}
        handleClickCancelCheckOut={handleClickCancelCheckOut}
        totalPrice={totalPrice} />
    );
  }

  static resolveEnabledIngredients(ingredients) {
    const selectedIngredientsArray = Object.entries(ingredients);
    return selectedIngredientsArray.reduce((result, [ingredientName, ingredientCount]) => {
      if (ingredientCount > 0) {
        const newIng = { [ingredientName]: true };
        return { ...result, ...newIng };
      }
      return result;
    }, {});
  }

  static resolveCheckoutPageLocation() {
    return { pathname: APP_LINKS.checkOut };
  }

  static routeToCheckoutPage(history) {
    history.push(BurgerBuilder.resolveCheckoutPageLocation());
  }

  componentDidMount() {
    BurgerBuilder.componentDidMountStatic(
      this.props.getInitialBuilderState,
      this.props.isAuthenticated,
      this.props.isBurgerInConfirmProcess,
      this.props.isBuild,
      this.props.setBurgerInConfirmProcess,
    );
  }

  handleClickOrderButton = () => {
    BurgerBuilder.handleClickOrderButton(
      this.props.setBurgerInConfirmProcess,
      this.props.isAuthenticated,
      this.props.history,
    );
  };

  handleClickContinueCheckOut = () => {
    BurgerBuilder.handleClickContinueCheckOut(
      this.props.setOrderInCheckoutProcess,
      this.props.history,
    );
  };

  handleClickCancelCheckOut = () => {
    BurgerBuilder.handleClickCancelCheckOut(this.props.setBurgerInConfirmProcess);
  };

  handleClickIngredientAdd = (ingredientName) => {
    BurgerBuilder.handleClickIngredientAdd(
      this.props.addIngredient,
      ingredientName,
    );
  };

  handleClickIngredientRemove = (ingredientName) => {
    BurgerBuilder.handleClickIngredientRemove(
      this.props.removeIngredient,
      ingredientName,
    );
  };

  render() {
    return (
      <>
        <Modal isModalVisible={this.props.isBurgerInConfirmProcess && this.props.isAuthenticated}
               handleClickCloseModal={this.handleClickCancelCheckOut}>
          {BurgerBuilder.getOrderSummaryNodes(
            this.props.ingredients,
            this.props.totalPrice,
            this.handleClickContinueCheckOut,
            this.handleClickCancelCheckOut,
          )}
        </Modal>
        {BurgerBuilder.getBurgerAndControlPanelNodes(
          this.props.error,
          this.props.ingredients,
          this.props.isAuthenticated,
          this.props.totalPrice,
          this.handleClickIngredientAdd,
          this.handleClickIngredientRemove,
          this.handleClickOrderButton,
        )}
      </>
    );
  }
}


/* -------------------------------------------------( exports )------------------------------------------------------ */
export default connect(mapStateToProps, mapDispatchToProps)(BurgerBuilder);
// export default connect(mapStateToProps, mapDispatchToProps)(withErrorHandler(BurgerBuilder, axios));
