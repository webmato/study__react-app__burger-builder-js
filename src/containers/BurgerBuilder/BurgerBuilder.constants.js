export const BURGER_DELIVERY_METHODS = {
  pickUp: 'Pick Up',
  deliveryCheapest: 'Cheapest',
  deliveryNormal: 'Normal',
  deliveryFastest: 'Fastest',
};

export default BURGER_DELIVERY_METHODS;
