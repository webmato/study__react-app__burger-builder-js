import * as PropTypes from 'prop-types';

export const ingredientsCountListPropsType = PropTypes.objectOf(PropTypes.number);
export const ingredientsEnabledListPropsType = PropTypes.objectOf(PropTypes.bool);
