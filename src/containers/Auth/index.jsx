/* -------------------------------------------------( imports )------------------------------------------------------ */
// Dependencies
import * as PropTypes from 'prop-types';
import * as React from 'react';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';

// Modules
import { APP_LINKS } from '../../App.constants';
import Button, { BUTTON_STYLES, BUTTON_TYPES } from '../../components/UI/Button';
import Spinner from '../../components/UI/Spinner';
import TextPanel from '../../components/UI/TextPanel';
import TEXT_PANEL_TYPE from '../../components/UI/TextPanel/TextPanel.constants';
import * as actions from '../../store/actions';
import { REGEXP_RULES, validateFormValue, VALIDATION_RULES } from '../utilities';
import FormElement, { ELEMENT_TYPES, INPUT_TYPES } from '../../components/UI/FormElement';

// Assets
import classes from './Auth.module.css';


/* ------------------------------------------------( variables )----------------------------------------------------- */
const mapStateToProps = state => ({
  isAuthenticated: !!state.authState.authData.token,
  isLoading: state.authState.isLoading,
  error: state.authState.error,
  errorData: state.authState.errorData,
});

const mapDispatchToProps = dispatch => ({
  initUserAuth: (email, password, isSignUpMode) => dispatch(actions.userAuthInit(email, password, isSignUpMode)),
});

/* -------------------------------------------------( methods )------------------------------------------------------ */


/* ------------------------------------------------( component )----------------------------------------------------- */
class Auth extends React.Component {
  static defaultProps = {
    errorData: null,
    isAuthenticated: false,
  };

  static propTypes = {
    error: PropTypes.bool.isRequired,
    errorData: PropTypes.shape({
      message: PropTypes.string,
    }),
    initUserAuth: PropTypes.func.isRequired,
    isLoading: PropTypes.bool.isRequired,
    isAuthenticated: PropTypes.bool,
  };

  static authenticateUser(email, password, isSignUpMode, initUserAuth) {
    initUserAuth(email, password, isSignUpMode);
  }

  static getRedirectNode(isAuthenticated) {
    return isAuthenticated ? (<Redirect to={APP_LINKS.burgerBuilder} />) : null;
  }

  static getErrorMessageNode(error, errorData) {
    if (error && errorData) {
      const { message } = errorData;
      return (<TextPanel type={TEXT_PANEL_TYPE.danger}>{message.replace(/_/gi, ' ')}</TextPanel>);
    }
    return null;
  }

  static getFormElementNode(orderFormKey, formElement, handleChangeForm) {
    const {
      formElementType,
      formElementConfig,
      isValid,
      isTouched,
      value,
      label,
    } = formElement;
    return (
      <FormElement key={orderFormKey}
                   elementType={formElementType}
                   elementConfig={formElementConfig}
                   value={value}
                   isTouched={isTouched}
                   isValid={isValid}
                   label={label}
                   handleChangeFormElement={evt => handleChangeForm(evt, orderFormKey)} />
    );
  }

  static getFormElementNodes(orderFormConfig, handleChangeForm) {
    return Object.entries(orderFormConfig).map((formElementEntry) => {
      const [orderFormKey, formElement] = formElementEntry;
      return Auth.getFormElementNode(orderFormKey, formElement, handleChangeForm);
    });
  }

  static handleSubmitAuthForm(evt, email, password, isSignUpMode, initUserAuth) {
    evt.preventDefault();
    Auth.authenticateUser(email, password, isSignUpMode, initUserAuth);
  }

  static nextStateIsSignUpMode(isSignUpMode) {
    return { isSignUpMode: !isSignUpMode };
  }

  static nextStateAuthFormIsValid(formElements) {
    const nextStateOrderFormIsValid = Object.values(formElements).reduce((prevValue, formElement) => {
      if (prevValue) {
        const { isValid } = formElement;
        if (isValid === undefined) {
          return prevValue;
        }
        return isValid && prevValue;
      }
      return false;
    }, true);
    return { formIsValid: nextStateOrderFormIsValid };
  }

  static nextStateFormElements(formElement, formElementKey, formElementsState) {
    const { value } = formElement;
    const { validationRules, isValid } = formElementsState[formElementKey];

    const isValidNextState = validationRules ? validateFormValue(value, validationRules) : isValid;

    return {
      formElements: {
        ...formElementsState,
        [formElementKey]: {
          ...formElementsState[formElementKey],
          value,
          isTouched: true,
          isValid: isValidNextState,
        },
      },
    };
  }

  static nextStateOnAuthFormChange(targetElement, formElementKey, formElementsState) {
    const formElementsNextState = Auth.nextStateFormElements(targetElement, formElementKey, formElementsState);
    const formIsValidNextState = Auth.nextStateAuthFormIsValid(formElementsNextState.formElements);

    return { ...formElementsNextState, ...formIsValidNextState };
  }

  state = {
    formElements: {
      email: {
        formElementType: ELEMENT_TYPES.input,
        formElementConfig: {
          id: 'email',
          type: INPUT_TYPES.email,
          placeholder: 'Your Email Address',
        },
        label: 'Email',
        isValid: false,
        isTouched: false,
        validationRules: {
          [VALIDATION_RULES.IS_REQUIRED]: true,
          [VALIDATION_RULES.REGEXP_RULE]: REGEXP_RULES.EMAIL,
        },
        value: '',
      },
      password: {
        formElementType: ELEMENT_TYPES.input,
        formElementConfig: {
          id: 'password',
          type: INPUT_TYPES.password,
          placeholder: 'Your Password',
        },
        label: 'Password',
        isValid: false,
        isTouched: false,
        validationRules: {
          [VALIDATION_RULES.IS_REQUIRED]: true,
          [VALIDATION_RULES.MIN_LENGTH]: 6,
          [VALIDATION_RULES.REGEXP_RULE]: REGEXP_RULES.NON_WHITE_CHARS,
        },
        value: '',
      },
    },
    formIsValid: false,
    isSignUpMode: false,
  };

  handleChangeAuthForm = (evt, authFormKey) => {
    const nextState = Auth.nextStateOnAuthFormChange(evt.target, authFormKey, this.state.formElements);
    //
    if (nextState) {
      this.setState(nextState);
    }
  };

  handleSubmitAuthForm = evt => Auth.handleSubmitAuthForm(
    evt,
    this.state.formElements.email.value,
    this.state.formElements.password.value,
    this.state.isSignUpMode,
    this.props.initUserAuth,
  );


  handleClickSwitchAuthMode = () => {
    const nextState = Auth.nextStateIsSignUpMode(this.state.isSignUpMode);
    //
    if (nextState) {
      this.setState(nextState);
    }
  };

  render() {
    if (this.props.isLoading) {
      return (<Spinner />);
    }

    return (
      <div className={classes.FormData}>
        <h3>Please {this.state.isSignUpMode ? 'Sign Up' : 'Log In'}</h3>
        {Auth.getRedirectNode(this.props.isAuthenticated)}
        {Auth.getErrorMessageNode(this.props.error, this.props.errorData)}

        <form onSubmit={this.handleSubmitAuthForm}>
          {Auth.getFormElementNodes(this.state.formElements, this.handleChangeAuthForm)}

          <Button buttonType={BUTTON_TYPES.submit}
                  isDisabled={!this.state.formIsValid}
                  buttonStyle={BUTTON_STYLES.success}>SUBMIT</Button>
        </form>

        <hr />
        <Button buttonStyle={BUTTON_STYLES.danger}
                handleClickButton={this.handleClickSwitchAuthMode}>
          SWITCH TO {this.state.isSignUpMode ? 'LOG IN' : 'SIGN UP'}
        </Button>
      </div>
    );
  }
}


/* -------------------------------------------------( exports )------------------------------------------------------ */
export default connect(mapStateToProps, mapDispatchToProps)(Auth);
