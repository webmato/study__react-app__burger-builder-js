/* -------------------------------------------------( imports )------------------------------------------------------ */
// Dependencies
import * as React from 'react';
import { connect } from 'react-redux';
import * as PropTypes from 'prop-types';
import Order from '../../components/Order';

// Modules
// import axios from '../../axios/axios-orders';
import Spinner from '../../components/UI/Spinner';
import TextPanel from '../../components/UI/TextPanel';
import TEXT_PANEL_TYPE from '../../components/UI/TextPanel/TextPanel.constants';
import * as actions from '../../store/actions';
// import withErrorHandler from '../../hoc/withErrorHandler';

// Assets


/* ------------------------------------------------( variables )----------------------------------------------------- */
const mapStateToProps = state => ({
  error: state.ordersState.error,
  errorData: state.ordersState.errorData,
  isLoading: state.ordersState.isLoading,
  orders: state.ordersState.orders,
  token: state.authState.authData.token,
  userId: state.authState.authData.userId,
});

const mapDispatchToProps = dispatch => ({
  getOrdersList: (token, userId) => dispatch(actions.ordersListGetInit(token, userId)),
});


/* -------------------------------------------------( methods )------------------------------------------------------ */


/* ------------------------------------------------( component )----------------------------------------------------- */
class Orders extends React.Component {
  static defaultProps = {
    errorData: null,
    token: null,
  };

  static propTypes = {
    error: PropTypes.bool.isRequired,
    errorData: PropTypes.shape({
      error: PropTypes.string,
    }),
    token: PropTypes.string,
    getOrdersList: PropTypes.func.isRequired,
    isLoading: PropTypes.bool.isRequired,
    orders: PropTypes.instanceOf(Map).isRequired,
    userId: PropTypes.string.isRequired,
  };

  static componentDidMountStatic(getOrdersList, token, userId) {
    getOrdersList(token, userId);
  }

  static getOrderNodes(orders, isLoading) {
    if (isLoading) {
      return <Spinner />;
    }

    if (orders && orders.size > 0) {
      return [...orders].map((order) => {
        const [orderId, orderData] = order;
        return <Order key={orderId} orderData={orderData} />;
      });
    }

    return (<div style={{ textAlign: 'center' }}><em><strong>No Orders Found!</strong></em></div>);
  }

  componentDidMount() {
    Orders.componentDidMountStatic(this.props.getOrdersList, this.props.token, this.props.userId);
  }

  render() {
    if (this.props.error) {
      return (<TextPanel type={TEXT_PANEL_TYPE.danger}>{this.props.errorData.error}</TextPanel>);
    }

    return (
      <section>
        {Orders.getOrderNodes(this.props.orders, this.props.isLoading)}
      </section>
    );
  }
}


/* -------------------------------------------------( exports )------------------------------------------------------ */
// export default withErrorHandler(Orders, axios);
export default connect(mapStateToProps, mapDispatchToProps)(Orders);
