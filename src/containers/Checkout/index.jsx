/* -------------------------------------------------( imports )------------------------------------------------------ */
// Dependencies
import * as PropTypes from 'prop-types';
import * as React from 'react';
import { connect } from 'react-redux';
import { Redirect, Route } from 'react-router-dom';
import { ingredientsCountListPropsType } from '../BurgerBuilder/BurgerBuilder.propTypes';

// Modules
import OrderForm from './OrderForm';
import OrderSummary from '../../components/Order/OrderSummary';
import { APP_LINKS } from '../../App.constants';

// Assets


/* ------------------------------------------------( variables )----------------------------------------------------- */
const mapStateToProps = state => ({
  ingredients: state.burgerBuilderState.ingredients,
  isLoading: state.ordersState.isLoading,
  isOrderInCheckOutProcess: state.ordersState.isOrderInCheckOutProcess,
  totalPrice: state.burgerBuilderState.totalPrice,
});


/* -------------------------------------------------( methods )------------------------------------------------------ */


/* ------------------------------------------------( component )----------------------------------------------------- */
class Checkout extends React.Component {
  static defaultProps = {
    ingredients: null,
    totalPrice: null,
  };

  static propTypes = {
    ingredients: ingredientsCountListPropsType,
    isLoading: PropTypes.bool.isRequired,
    isOrderInCheckOutProcess: PropTypes.bool.isRequired,
    location: PropTypes.shape({
      search: PropTypes.string,
    }).isRequired,
    history: PropTypes.shape({
      goBack: PropTypes.func,
      push: PropTypes.func,
    }).isRequired,
    totalPrice: PropTypes.number,
  };

  static orderCancel(history) {
    history.goBack();
  }

  static orderContinue(history) {
    history.push(APP_LINKS.checkOutContactForm);
  }

  handleClickOrderContinue = () => Checkout.orderContinue(this.props.history);

  handleClickOrderCancel = () => Checkout.orderCancel(this.props.history);

  render() {
    if (!this.props.isOrderInCheckOutProcess) {
      return (<Redirect to={APP_LINKS.orders} />);
    }

    if (this.props.ingredients && this.props.totalPrice) {
      return (
        <div>
          <OrderSummary ingredients={this.props.ingredients}
                        isLoading={this.props.isLoading}
                        totalPrice={this.props.totalPrice}
                        handleClickOrderCancel={this.handleClickOrderCancel}
                        handleClickOrderContinue={this.handleClickOrderContinue} />
          <Route path={APP_LINKS.checkOutContactForm}
                 component={OrderForm} />
        </div>
      );
    }

    return (<Redirect to={APP_LINKS.burgerBuilder} />);
  }
}


/* -------------------------------------------------( exports )------------------------------------------------------ */
export default connect(mapStateToProps)(Checkout);
