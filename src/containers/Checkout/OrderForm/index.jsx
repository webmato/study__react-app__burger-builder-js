/* -------------------------------------------------( imports )------------------------------------------------------ */
// Dependencies
import * as PropTypes from 'prop-types';
import * as React from 'react';
import { connect } from 'react-redux';

// Modules
// import axios from '../../../axios/axios-orders';
import Button, { BUTTON_STYLES } from '../../../components/UI/Button';
import FormElement, { ELEMENT_TYPES, INPUT_TYPES } from '../../../components/UI/FormElement';
import Spinner from '../../../components/UI/Spinner';
// import withErrorHandler from '../../../hoc/withErrorHandler';
import * as actions from '../../../store/actions';
import { BURGER_DELIVERY_METHODS } from '../../BurgerBuilder/BurgerBuilder.constants';
import { ingredientsCountListPropsType } from '../../BurgerBuilder/BurgerBuilder.propTypes';
import { REGEXP_RULES, validateFormValue, VALIDATION_RULES } from '../../utilities';

// Assets
import classes from './OrderForm.module.css';


/* ------------------------------------------------( variables )----------------------------------------------------- */
const mapStateToProps = state => ({
  token: state.authState.authData.token,
  ingredients: state.burgerBuilderState.ingredients,
  userId: state.authState.authData.userId,
  totalPrice: state.burgerBuilderState.totalPrice,
  isLoading: state.ordersState.isLoading,
});

const mapDispatchToProps = dispatch => ({
  burgerBuilderReset: () => dispatch(actions.builderInitialStateRequest()),
  burgerOrderSend: (orderData, token) => dispatch(actions.orderCheckoutInit(orderData, token)),
});

export function resolveNextStateOrderFormIsValid(formElements) {
  const nextStateOrderFormIsValid = Object.values(formElements).reduce((prevValue, formElement) => {
    if (prevValue) {
      const { isValid } = formElement;
      if (isValid === undefined) {
        return prevValue;
      }
      return isValid && prevValue;
    }
    return false;
  }, true);
  return { orderFormIsValid: nextStateOrderFormIsValid };
}

export function resolveNextStateOrderFormElements(formElement, orderFormKey, prevStateOrderForm) {
  const { value } = formElement;
  const { validationRules, isValid } = prevStateOrderForm[orderFormKey];

  const isValidNextState = validationRules ? validateFormValue(value, validationRules) : isValid;

  return {
    orderFormElements: {
      ...prevStateOrderForm,
      [orderFormKey]: {
        ...prevStateOrderForm[orderFormKey],
        value,
        isTouched: true,
        isValid: isValidNextState,
      },
    },
  };
}

export function getFormElementNode(orderFormKey, formElement, handleChangeFormElement) {
  const {
    formElementType,
    formElementConfig,
    isValid,
    isTouched,
    value,
    label,
  } = formElement;
  return (
    <FormElement key={orderFormKey}
                 elementType={formElementType}
                 elementConfig={formElementConfig}
                 value={value}
                 isTouched={isTouched}
                 isValid={isValid}
                 label={label}
                 handleChangeFormElement={evt => handleChangeFormElement(evt, orderFormKey)} />
  );
}

export function getFormElementNodes(orderFormConfig, handleChangeFormElement) {
  return Object.entries(orderFormConfig).map((formElementEntry) => {
    const [orderFormKey, formElement] = formElementEntry;
    return getFormElementNode(orderFormKey, formElement, handleChangeFormElement);
  });
}

export function getOrderFormNode(
  orderFormConfig,
  orderFormIsValid,
  isLoading,
  handleChangeFormElement,
  handleClickSubmitOrder,
) {
  const { deliveryMethod, ...restOrderFormConfig } = orderFormConfig;

  if (isLoading) {
    return (<Spinner />);
  }

  return (
    <form>
      <h3>Please select Delivery Method</h3>
      {getFormElementNodes({ deliveryMethod }, handleChangeFormElement)}

      <h3>Please enter your Contact Data</h3>
      {getFormElementNodes(restOrderFormConfig, handleChangeFormElement)}

      <Button buttonStyle={BUTTON_STYLES.success}
              handleClickButton={handleClickSubmitOrder}
              isDisabled={!orderFormIsValid}>SUBMIT ORDER</Button>
    </form>
  );
}


/* ------------------------------------------------( component )----------------------------------------------------- */
class OrderForm extends React.Component {
  static defaultProps = {
    token: null,
    ingredients: null,
    totalPrice: null,
  };

  static propTypes = {
    burgerBuilderReset: PropTypes.func.isRequired,
    burgerOrderSend: PropTypes.func.isRequired,
    token: PropTypes.string,
    ingredients: ingredientsCountListPropsType,
    isLoading: PropTypes.bool.isRequired,
    totalPrice: PropTypes.number,
    userId: PropTypes.string.isRequired,
  };

  static composeAnOrderData(orderForm, ingredients, totalPrice, userId) {
    const orderDetails = Object.entries(orderForm).reduce(
      (currentOrderDetails, formElementEntry) => {
        const [formElementKey, formElementData] = formElementEntry;
        const { value } = formElementData;
        return { [formElementKey]: value, ...currentOrderDetails };
      },
      {},
    );

    return {
      ingredients,
      totalPrice,
      orderDetails,
      userId,
      date: Date.now(),
    };
  }

  static handleChangeFormElement(evt, orderFormKey, orderFormElements) {
    const nextStateOrderFormElements = resolveNextStateOrderFormElements(
      evt.target,
      orderFormKey,
      orderFormElements,
    );
    const nextStateOrderFormIsValid = resolveNextStateOrderFormIsValid(nextStateOrderFormElements.orderFormElements);

    return { ...nextStateOrderFormElements, ...nextStateOrderFormIsValid };
  }

  static handleClickSubmitOrder(
    orderFormElements,
    token,
    ingredients,
    totalPrice,
    burgerBuilderReset,
    burgerOrderSend,
    userId,
  ) {
    const burgerOrderData = OrderForm.composeAnOrderData(orderFormElements, ingredients, totalPrice, userId);
    burgerOrderSend(burgerOrderData, token);
    burgerBuilderReset();
  }

  state = {
    orderFormElements: {
      name: {
        formElementType: ELEMENT_TYPES.input,
        formElementConfig: {
          id: 'orderName',
          type: INPUT_TYPES.text,
          placeholder: 'Your Name',
        },
        label: 'Name',
        isValid: false,
        isTouched: false,
        validationRules: {
          [VALIDATION_RULES.IS_REQUIRED]: true,
        },
        value: '',
      },
      email: {
        formElementType: ELEMENT_TYPES.input,
        formElementConfig: {
          id: 'orderEmail',
          type: INPUT_TYPES.email,
          placeholder: 'Your Email Address',
        },
        label: 'Email',
        isValid: false,
        isTouched: false,
        validationRules: {
          [VALIDATION_RULES.IS_REQUIRED]: true,
          [VALIDATION_RULES.REGEXP_RULE]: REGEXP_RULES.EMAIL,
        },
        value: '',
      },
      address_street: {
        formElementType: ELEMENT_TYPES.input,
        formElementConfig: {
          id: 'orderAddrStreet',
          type: INPUT_TYPES.text,
          placeholder: 'Address Street',
        },
        label: 'Street',
        isValid: false,
        isTouched: false,
        validationRules: {
          [VALIDATION_RULES.IS_REQUIRED]: true,
        },
        value: '',
      },
      address_town: {
        formElementType: ELEMENT_TYPES.input,
        formElementConfig: {
          id: 'orderAddrTown',
          type: INPUT_TYPES.text,
          placeholder: 'Address City/Town',
        },
        label: 'City/Town',
        isValid: false,
        isTouched: false,
        validationRules: {
          [VALIDATION_RULES.IS_REQUIRED]: true,
        },
        value: '',
      },
      address_postalCode: {
        formElementType: ELEMENT_TYPES.input,
        formElementConfig: {
          id: 'orderAddrPostalCode',
          type: INPUT_TYPES.text,
          placeholder: 'Address Postal Code',
        },
        label: 'Postal Code',
        isValid: false,
        isTouched: false,
        validationRules: {
          [VALIDATION_RULES.IS_REQUIRED]: true,
          [VALIDATION_RULES.MIN_LENGTH]: 4,
          [VALIDATION_RULES.MAX_LENGTH]: 6,
          [VALIDATION_RULES.REGEXP_RULE]: REGEXP_RULES.DIGITS_ONLY,
        },
        value: '',
      },
      address_country: {
        formElementType: ELEMENT_TYPES.input,
        formElementConfig: {
          id: 'orderAddrCountry',
          type: INPUT_TYPES.text,
          placeholder: 'Country',
        },
        label: 'Country',
        isValid: false,
        isTouched: false,
        validationRules: {
          [VALIDATION_RULES.IS_REQUIRED]: true,
        },
        value: '',
      },
      deliveryMethod: {
        formElementType: ELEMENT_TYPES.select,
        formElementConfig: {
          id: 'deliveryMethod',
          options: BURGER_DELIVERY_METHODS,
        },
        label: null,
        isValid: true,
        isTouched: false,
        validationRules: {},
        value: Object.entries(BURGER_DELIVERY_METHODS)
        // Get a key name
          .find(([key, val]) => val === BURGER_DELIVERY_METHODS.deliveryCheapest)[0], // eslint-disable-line no-unused-vars
      },
    },
    orderFormIsValid: false,
  };

  handleChangeFormElement = (evt, orderFormKey) => {
    const nextState = OrderForm.handleChangeFormElement(evt, orderFormKey, this.state.orderFormElements);
    if (nextState) {
      this.setState(nextState);
    }
  };

  handleClickSubmitOrder = () => {
    OrderForm.handleClickSubmitOrder(
      this.state.orderFormElements,
      this.props.token,
      this.props.ingredients,
      this.props.totalPrice,
      this.props.burgerBuilderReset,
      this.props.burgerOrderSend,
      this.props.userId,
    );
  };

  render() {
    return (
      <div className={classes.FormData}>
        {getOrderFormNode(
          this.state.orderFormElements,
          this.state.orderFormIsValid,
          this.props.isLoading,
          this.handleChangeFormElement,
          this.handleClickSubmitOrder,
        )}
      </div>
    );
  }
}


/* -------------------------------------------------( exports )------------------------------------------------------ */
export default connect(mapStateToProps, mapDispatchToProps)(OrderForm);
// export default connect(mapStateToProps, mapDispatchToProps)(withErrorHandler(OrderForm, axios));
