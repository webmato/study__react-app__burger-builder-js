/* -------------------------------------------------( imports )------------------------------------------------------ */
// Dependencies
import * as PropTypes from 'prop-types';
import * as React from 'react';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';

// Modules
import { APP_LINKS } from '../../App.constants';
import * as actions from '../../store/actions';

// Assets


/* ------------------------------------------------( variables )----------------------------------------------------- */
const mapDispatchToProps = dispatch => ({
  userLogOut: () => dispatch(actions.userLogOut()),
});

/* -------------------------------------------------( methods )------------------------------------------------------ */


/* ------------------------------------------------( component )----------------------------------------------------- */
class LogOut extends React.Component {
  static defaultProps = {
    userLogOut: null,
  };

  static propTypes = {
    userLogOut: PropTypes.func,
  };

  state = {};

  componentDidMount() {
    this.props.userLogOut();
  }

  render() {
    return (<Redirect to={APP_LINKS.burgerBuilder} />);
  }
}


/* -------------------------------------------------( exports )------------------------------------------------------ */
export default connect(null, mapDispatchToProps)(LogOut);
