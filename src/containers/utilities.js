export const VALIDATION_RULES = {
  MAX_LENGTH: 'maxLength',
  MIN_LENGTH: 'minLength',
  IS_REQUIRED: 'isRequired',
  REGEXP_RULE: 'regexp',
  ALLOWED_CHARS: 'allowedChars',
};

export const REGEXP_RULES = {
  EMAIL: /^([a-z0-9]+)((\.[a-z0-9]+)*([a-z0-9\-_]*)*([a-z0-9])+)*@(([a-z0-9]([a-z0-9-]*[a-z0-9])?)+\.)+([a-z]{2,})$/i, // eslint-disable-line max-len
  DIGITS_ONLY: /^\d+$/,
  NON_WHITE_CHARS: /^(\S)*$/, // a-zA-Z0-9"!"#$%&'()*+,-./:;<=>?@[\]^_`{|}~"...
};

export function validateRegExp(testedValue, regExp) {
  return regExp.test(testedValue);
}

export function tester(testedValue, [ruleName, ruleValue]) {
  switch (ruleName) {
    case VALIDATION_RULES.IS_REQUIRED: { return !!testedValue.trim(); }
    case VALIDATION_RULES.MIN_LENGTH: { return testedValue.trim().length >= ruleValue; }
    case VALIDATION_RULES.MAX_LENGTH: { return testedValue.trim().length <= ruleValue; }
    case VALIDATION_RULES.ALLOWED_CHARS: { return validateRegExp(testedValue, new RegExp(`^([${ruleValue}]*)$`)); }
    case VALIDATION_RULES.REGEXP_RULE: { return validateRegExp(testedValue, ruleValue); }
    default: { return true; }
  }
}

export function validateFormValue(testedValue, validationRules) {
  return Object.entries(validationRules).reduce(
    (currentValidity, rule) => {
      const result = tester(testedValue, rule);
      return result && currentValidity;
    },
    true,
  );
}
