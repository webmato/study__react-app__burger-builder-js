import axios from 'axios';
import { API_BASE_URL } from '../App.constants';

const instance = axios.create({
  baseURL: API_BASE_URL.database,
});

export default instance;
