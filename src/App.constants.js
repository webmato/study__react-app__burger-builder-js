const APP_LINKS = {
  auth: '/auth',
  burgerBuilder: '/',
  logOut: '/logout',
  checkOut: '/checkout',
  checkOutContactForm: '/checkout/contact',
  orders: '/orders',
};

const APP_LINKS__VALUES = Object.values(APP_LINKS);

const API_URL = {
  order: {
    POST: '/burger-orders.json',
  },
  settings: {
    GET: '/settings.json',
  },
  ordersList: {
    GET: '/burger-orders.json',
  },
};

const FIREBASE_CONFIG = {
  apiKey: 'AIzaSyCkpC0fdw3eeW_Qcx9ws8lQTGTQL4AAxRs',
  authDomain: 'burger-builder-f5780.firebaseapp.com',
  databaseURL: 'https://burger-builder-f5780.firebaseio.com',
  projectId: 'burger-builder-f5780',
  storageBucket: 'burger-builder-f5780.appspot.com',
  messagingSenderId: '68819113249',
};

const API_BASE_URL = {
  auth: {
    // https://firebase.google.com/docs/reference/rest/auth/
    email: {
      signUp: `https://www.googleapis.com/identitytoolkit/v3/relyingparty/signupNewUser?key=${FIREBASE_CONFIG.apiKey}`,
      logIn: `https://www.googleapis.com/identitytoolkit/v3/relyingparty/verifyPassword?key=${FIREBASE_CONFIG.apiKey}`,
    },
  },
  database: FIREBASE_CONFIG.databaseURL,
};

const LOCAL_STORAGE_KEYS = {
  expirationDate: 'expirationDate',
  token: 'token',
  userId: 'userId',
};

export {
  APP_LINKS,
  APP_LINKS__VALUES,
  API_URL,
  API_BASE_URL,
  LOCAL_STORAGE_KEYS,
};
